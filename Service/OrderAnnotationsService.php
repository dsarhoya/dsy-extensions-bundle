<?php

namespace dsarhoya\DSYExtensionsBundle\Service;

use Metadata\MetadataFactoryInterface;
use Doctrine\ORM\EntityManager;
use Symfony\Component\PropertyAccess\PropertyAccess;

class OrderAnnotationsService 
{
    private $metadataFactory;
    private $em;
    private $accessor;

    public function __construct(MetadataFactoryInterface $metadataFactory, EntityManager $em)
    {
        $this->metadataFactory  = $metadataFactory;
        $this->em               = $em;
        $this->accessor         = PropertyAccess::createPropertyAccessor();
    }
    
    public function orderGroupConstants($object){
        $classMetadata = $this->metadataFactory->getMetadataForClass(is_object($object) ? get_class($object) : $object);
        return $classMetadata->constans;
    }
    
    public function orderProperty($object){
        $classMetadata = $this->metadataFactory->getMetadataForClass(is_object($object) ? get_class($object) : $object);
        return $classMetadata->order_property;
    }
    
    public function moveUp($object){
        $brothers = $this->getBrothers($object);
        if($brothers[0]->getId() === $object->getId()) return;
        
        foreach ($brothers as $index=>$brother){
            if($brother->getId() === $object->getId()){
                
                $this->accessor->setValue($brothers[$index-1], 
                                            $this->orderProperty($brothers[$index-1]), 
                                            $this->accessor->getValue($brothers[$index-1], $this->orderProperty($brothers[$index-1]))+1);
                $this->accessor->setValue($object, 
                                            $this->orderProperty($object), 
                                            $this->accessor->getValue($object, $this->orderProperty($object))-1);
            }
        }
        $this->em->flush();
    }
    
    public function moveDown($object){
        $brothers = $this->getBrothers($object);
        if($brothers[count($brothers)-1]->getId() === $object->getId()) return;
        
        foreach ($brothers as $index=>$brother){
            if($brother->getId() === $object->getId()){
                
                $this->accessor->setValue($brothers[$index+1], 
                                            $this->orderProperty($brothers[$index+1]), 
                                            $this->accessor->getValue($brothers[$index+1], $this->orderProperty($brothers[$index+1]))-1);
                $this->accessor->setValue($object, 
                                            $this->orderProperty($object), 
                                            $this->accessor->getValue($object, $this->orderProperty($object))+1);
            }
        }
        $this->em->flush();
    }
    
    public function moveTop($object){
        $brothers = $this->getBrothers($object);
        if($brothers[0]->getId() === $object->getId()) return;
        
        foreach ($brothers as $brother){
            $this->accessor->setValue($brother, 
                                        $this->orderProperty($brother), 
                                        $this->accessor->getValue($brother, $this->orderProperty($brother))+1);
            
            if($brother->getId() === $object->getId()){
                break;
            }
        }
        
        $this->accessor->setValue($object, 
                                    $this->orderProperty($object), 
                                    1);
        $this->em->flush();
    }
    
    public function moveBottom($object){
        $brothers = $this->getBrothers($object);
        if($brothers[count($brothers)-1]->getId() === $object->getId()) return;
        
        $start = false;
        foreach ($brothers as $brother){
            if($brother->getId() === $object->getId()){
                $start = true;
            }
            
            if(!$start) continue;

            $this->accessor->setValue($brother, 
                                        $this->orderProperty($brother), 
                                        $this->accessor->getValue($brother, $this->orderProperty($brother))-1);
        }
        
        $this->accessor->setValue($object, 
                                    $this->orderProperty($object), 
                                    count($brothers));
        
        $this->em->flush();
    }
    
    public function normalize($class_name){
        $brothers = $this->getBrothers($class_name);
        $order = 1;
        foreach ($brothers as $brother) {
            $this->accessor->setValue($brother, $this->orderProperty($class_name), $order);
            $order++;
        }
        $this->em->flush();
    }
    
    private function getBrothers($object){
        $repo = $this->em->getRepository(is_object($object) ? get_class($object) : $object);
        
        $constants = array();
        foreach ($this->orderGroupConstants($object) as $constant_name) {
            $constants[$constant_name] = $this->accessor->getValue($object, $constant_name);
        }
        
        return $repo->findBy($constants, array(
            $this->orderProperty($object)=>'ASC'
        ));
    }
}
