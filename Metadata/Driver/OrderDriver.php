<?php

namespace dsarhoya\DSYExtensionsBundle\Metadata\Driver;

use Metadata\Driver\DriverInterface;
use Metadata\MergeableClassMetadata;
use Doctrine\Common\Annotations\Reader;
use dsarhoya\DSYExtensionsBundle\Metadata\OrderMetadata;

class OrderDriver implements DriverInterface
{
    private $reader;

    public function __construct(Reader $reader)
    {
        $this->reader = $reader;
    }

    public function loadMetadataForClass(\ReflectionClass $class)
    {
        $classMetadata = new OrderMetadata($class->getName());
        
        foreach ($class->getProperties() as $reflectionProperty) {

            $constant_annotation = $this->reader->getPropertyAnnotation(
                $reflectionProperty,
                'dsarhoya\\DSYExtensionsBundle\\Annotation\\OrderConstant'
            );

            $property_annotation = $this->reader->getPropertyAnnotation(
                $reflectionProperty,
                'dsarhoya\\DSYExtensionsBundle\\Annotation\\OrderProperty'
            );
            
            if (null !== $property_annotation){
                $classMetadata->setOrderPropertyName($reflectionProperty->getName());
            }elseif (null !== $constant_annotation) {
                $classMetadata->addConstantNamed($reflectionProperty->getName());
            }
        }
        
        return $classMetadata;
    }
}