<?php

namespace dsarhoya\DSYExtensionsBundle\Metadata;

use Metadata\MergeableClassMetadata;

class OrderMetadata extends MergeableClassMetadata
{
    public $constans = array();
    public $order_property = null;
    
    public function addConstantNamed($constant_name){
        $this->constans[] = $constant_name;
    }
    
    public function setOrderPropertyName($name){
        $this->order_property = $name;
    }
}